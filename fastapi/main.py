''' FastAPI '''

from os import environ
from re import match

from pymongo import AsyncMongoClient

from fastapi import FastAPI

app = FastAPI()
db = AsyncMongoClient(
    f"mongodb://mongoread:mongopass@{environ['MONGO_HOST']}/CN").CN


@app.get('/example')
async def example():
    ''' Sample JSON '''
    data = []
    async for document in db['example'].find():
        data.append({'date': document['date'], 'value': document['value']})
    return data


@app.get('/rrt')
async def rrt():
    ''' Mirror the RRT collection '''
    data = []
    async for document in db['RRT'].find(sort=[('started', -1)]):
        data.append(document)
    return data


@app.get('/report/{commit}')
async def report(commit: str):
    ''' Gather results for the given hash '''
    if len(commit) < 7:
        return []

    query = {'hash': {'$regex': '^' + commit[:7]}}
    sort = ['profile', 'set', 'note', ('started', -1)]

    data_smsf = []
    data_other = []
    data_mcs = []
    async for document in db['Results'].find(query, sort=sort):
        if document['profile'] == 'SMSF':
            data_smsf.append(document)
        elif document['profile'] == 'MCS':
            data_mcs.append(document)
        else:
            data_other.append(document)

    results = []
    for data in data_smsf + data_other + data_mcs:
        known_failures = await count_known_failures(data)

        passed = int(data['passed'])
        total = int(data['total'])
        minutes = int(data['minutes'])

        name = data['profile']
        if all('fc001216' in test['tags'] or 'ipv6_sanity' in test['tags']
               for test in data['tests']):
            name += ' IPv6'
        if data['note']:
            name += f" {data['note']}"
        name += f" {data['set'].upper()} {passed}/{total}"

        results.append({
            'name': name,
            'passed': passed,
            'failed': total - passed - known_failures,
            'known failures': known_failures,
            'minutes': minutes,
            'started': data['started'],
            'job': data['_id']
        })
    return results


async def count_known_failures(data):
    ''' Sum up the failed test cases which were reported to be known issues '''
    profile = data['profile'].lower()
    branch = data['branch']

    count = 0
    for test in data['tests']:
        if test['result'] == 'FAIL':
            if await is_known_issue(test, profile, branch):
                count += 1
    return count


async def is_known_issue(test, profile, branch):
    ''' Check if a test case is reported as known issue in the database '''
    known_issue = await db['Known-Issues'].find_one({'_id': test['name']})
    if not known_issue:
        return False

    if profile not in known_issue.get('profiles', []):
        return False

    if '_' in branch:
        release = branch.split('_')[0]
    elif match(r'ntas-\d+-\d+$', branch):
        release = branch
    else:
        release = 'master'

    if release not in known_issue.get('releases', []):
        return False

    return True
